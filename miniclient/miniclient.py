import socket
import os
import re

def send_image(
        data : bytes,
        server_address : (str, int)) -> None:
    """
    Sends the image to the visualization server.
    """

    print(f"Sending {len(data)} bytes to {server_address[0]}:{server_address[1]}...")
    max_attempts = 5
    attempt = 1
    while attempt <= max_attempts:
        try:
           tcp_socket = socket.create_connection(server_address)
           tcp_socket.sendall(bytes(data))
           tcp_socket.close()
        except Exception as ex:
            print(f"Sending image failed. Reattempting. ({attempt}/{max_attempts})\n{ex}")
            attempt += 1
        else:
            print("Image sent.")
            break

if __name__ == "__main__":
    while True:
        print("Messages available:")
        avail = [x for x in os.listdir("./messages/") if re.search("^fish-\d+.sltpmsg$", x)]
        for f in avail:
            print(f)

        while True:
            ftype_s = input("Fake scan; select number: ")
            try:
                ftype = int(ftype_s)
                fname = f"fish-{ftype}.sltpmsg"
                if fname not in avail:
                    print("This file does not exist.")
            except:
                print("Invalid input! Retry!")
            else:
                break

        with open(f"./messages/{fname}", "rb") as f:
            data = f.read()

        print(f"Sending recorded message from file {fname}")

        send_image(data, ("localhost", 4242))
