#!/bin/python

import config

# General
import time
from datetime import datetime

# image processing
from PIL import Image
import math

# gpio
if config.use_gpio:
    import RPi.GPIO as GPIO

# scanning
if config.use_scanner:
    import PIL
    import sane

# TCP/IP
import socket

# QR Codes
from pyzbar.pyzbar import decode as qr_decode
from pyzbar.pyzbar import Decoded as QrCode

from sltp import sltp_serialize

def log(message):
    "Prints a log message with time stamp"

    print(
        datetime.now().strftime("%Y/%m/%d %H:%M:%S")
        + ": "
        + message)

class FishType():
    """Represents a fish type"""

    def __init__(self,
            id : int,
            qr_ident : str,
            stencil : Image.Image,
            bounding_box : (int,int,int,int),
            qrcodes_xy : ((int,int),(int,int))):
        """
        id: unique fish type id
        stencil: The stencil image (uncropped!)
        bounding_box: (left, top, right, bottom)
        qrcodes_xy: *center* points of L and R qr codes (see remark)

        Remark:
        Both the bounding_box and the qrcodes_xy parameters apply to the
        ideal template, which means, that the stencil should be applied
        to an image which has the qr codes at the specified points.
        Of course, the scanned image will differ.
        Therefore, the qr-code coordinates can be used to rotate and
        shift the scanned image until *its* qr-codes are in the correct
        spot.
        The stencil can *then* be applied to it.

        Note that the stencil needs to have the same size as the original
        template.
        It will be cropped to the bounging box by this code.
        """
        self.id : int = id
        "Unique fish type id"
        self.qr_ident : str= qr_ident
        "The prefix of the data that needs to be in all QR codes"

        # Image mode needs to be b/w only with no alpha.
        # See image modes:
        # https://pillow.readthedocs.io/en/stable/handbook/concepts.html#concept-modes
        if not stencil.mode in "1L":
            raise ValueError(f"Stencil for type {id} in wrong image mode. Mode: {stencil.mode}")

        self.stencil : Image.Image = stencil.crop(bounding_box)
        "The fish type's stencil image"
        self.bounding_box : (int, int, int, int) = bounding_box
        "Bounding box around the actual fish image (L, T, R, B)"

        self.qr_xy_l : (int,int) = qrcodes_xy[0]
        "Center point of the 'L' qr code."
        self.qr_xy_r : (int,int) = qrcodes_xy[1]
        "Center point of the 'R' qr code."

        tan = ((self.qr_xy_r[1] - self.qr_xy_l[1])
                / (self.qr_xy_r[0] - self.qr_xy_l[0]))
        self.phi = math.atan(tan) / math.pi * 180 # RAD->DEG, tan is pi-periodic
        "Angle between the center points of the qr codes."

    def get_qr_dist(self):
        "Calculates the distance in pixels between the qr codes."
        delta_x = self.qr_xy_r[0] - self.qr_xy_l[0]
        delta_y = self.qr_xy_r[1] - self.qr_xy_l[1]
        return math.sqrt(delta_x**2 + delta_y**2)


fish_types = {
    1: FishType(
        id=1,
        qr_ident="F-001",
        stencil=Image.open("./stencils/fish-1.png"),
        bounding_box=(90,175,820,500),
        qrcodes_xy=((58,58),(819,562))),
    2: FishType(
        id=2,
        qr_ident="F-002",
        stencil=Image.open("./stencils/fish-2.png"),
        bounding_box=(50, 195, 855, 450),
        qrcodes_xy=((58,58),(819,562))),
    3: FishType(
        id=3,
        qr_ident="F-003",
        stencil=Image.open("./stencils/fish-3.png"),
        bounding_box=(80, 60, 820, 550),
        qrcodes_xy=((58,58),(819,562))),
    4: FishType(
        id=4,
        qr_ident="F-004",
        stencil=Image.open("./stencils/fish-4.png"),
        bounding_box=(115, 155, 767, 484),
        qrcodes_xy=((58,58),(819,562))),
    99: FishType(
        id=99,
        qr_ident="F-099",
        stencil=Image.open("./stencils/fish-99.png"),
        bounding_box=(130, 140, 757, 652),
        qrcodes_xy=((58,58),(819,562)))
}

def initialize_gpio():
    "Initializes the Input/Output modes of the GPIO pins."
    GPIO.setmode(GPIO.BOARD)
    GPIO.setup(config.gpio_button_pin, GPIO.IN)
    GPIO.setup(config.gpio_led_pin, GPIO.OUT)
    log("GPIO initialized")

def initialize_scanner():
    """
    Initializes the script, sets up GPIO pins, etc...
    Returns scanner device
    """
    sane.init()
    log("SANE initialized. Looking for scanner devices...")
    scan_device = None
    for f in sane.get_devices():
        if f[3] == config.scanner_dev:
            scan_device = f[0]
            break

    if scan_device == None:
        raise IOError("Could not find the scanning device")

    log(f"Found scanner device: '{scan_device}', opening...")

    dev = sane.open(scan_device)
    dev.mode = "Color"
    dev.resolution = 75 # 300dpi

    log("Scanner configured.")
    return dev

def set_led(on : bool) -> None:
    "Turn the status LED on or off."
    if not config.use_gpio:
        return
    if on:
        GPIO.output(config.gpio_led_pin, GPIO.HIGH)
    else:
        GPIO.output(config.gpio_led_pin, GPIO.LOW)

def await_scanner_triggered() -> None:
    """
    Wait for the button or similar that triggers the scan.
    """
    if not config.use_gpio:
        input("Please press [ENTER].")
        return

    set_led(True)
    log("Awaiting button press...")

    # We require the button to be released before recognising
    # a button press to prevent a stuck button from triggering
    # continous scanning
    button_released = False
    while True:
        if GPIO.input(config.gpio_button_pin) == GPIO.HIGH:
            button_released = True
        elif button_released:
            break

        time.sleep(0.1)
    log("Button pressed")
    set_led(False)

def rotate_img_180(img : Image.Image) -> Image.Image:
    "Returns the image rotated by 90 degree."
    if PIL.__version__ >= "9.1.0":
        return img.transpose(Image.Transpose.ROTATE_180)
    return img.transpose(Image.ROTATE_180)

def rotate_img_90(img : Image.Image) -> Image.Image:
    "Returns the image rotated by 90 degree."
    if PIL.__version__ >= "9.1.0":
        return img.transpose(Image.Transpose.ROTATE_90)
    return img.transpose(Image.ROTATE_90)

def scan_image(scanner) -> Image.Image:
    """
    Scans an image from a scanner
    """
    log("Scanning image...")
    img = scanner.scan()
    return rotate_img_90(img)

def fake_scan() -> Image.Image:
    """
    Loads a scanned image from HDD.
    """
    while True:
        ftype_s = input("Fake scan; select fish type [1-4,99]: ")
        try:
            ftype = int(ftype_s)
        except:
            print("Invalid input! Retry!")
        else:
            break
    return Image.open(f"testdata/scan-{ftype}-1.png")

def identify_image(img : Image.Image) -> (FishType,(int,int),(int,int)):
    """
    Reads the QR code on the template and identifies the fish type.
    Returns the type number and two XY tuples containing the centers
    of the left and the right qr code.
    """

    def get_qr_center(qr : QrCode) -> (int,int):
        "Gets the center of the qr code as (x,y)"
        r = qr.rect
        x, y, w, h = r.left, r.top, r.width, r.height
        return (x+w/2, y+h/2)

    img = img.copy()

    codes = qr_decode(img)
    if len(codes) != 2:
        raise RuntimeError(f"Unexpected code count: {len(codes)}")

    if codes[0].data[:5] != codes[1].data[:5]:
        raise RuntimeError("QR codes don't match!")

    d_str1 : str = codes[0].data.decode("ascii")
    d_str2 : str = codes[1].data.decode("ascii")

    # coordinates of the left and the right qr code.
    l_xy : (int, int)
    r_xy : (int, int)

    if d_str1.endswith("L"):
        l_xy = get_qr_center(codes[0])
    elif d_str2.endswith("L"):
        l_xy = get_qr_center(codes[1])
    else:
        raise RuntimeError("No left QR code found!")

    if d_str1.endswith("R"):
        r_xy = get_qr_center(codes[0])
    elif d_str2.endswith("R"):
        r_xy = get_qr_center(codes[1])
    else:
        raise RuntimeError("No right QR code found!")

    log(f"Found QR codes: '{d_str1}' and '{d_str2}'")

    for f_type in fish_types.values():
        if d_str1.startswith(f_type.qr_ident):
            log(f"Identified fish type: {f_type.id}")
            return (f_type, l_xy, r_xy)

    raise RuntimeError("No matching fish type!")

def cut_image(
        img : Image.Image,
        fish_type : FishType,
        l_xy : (int, int),
        r_xy : (int, int)) -> Image.Image:
    """
    Cuts the image based on the type stencil and the offset.
    Returns a *new* Image!
    img: Originial scanned image
    l_xy, r_xy: *center* points of the L and R qr codes.
    """

    def get_alpha(pixel):
        if(len(pixel) != 4):
            return 255
        return pixel[3]

    stencil = fish_type.stencil

    sw, sh = stencil.size
    iw, ih = img.size

    # Convert scan to RGB, so we can add the alpha layer from the stencil.
    img.convert("RGB")

    # Get the phi-offset = the image's rotation
    tan = (r_xy[1]-l_xy[1]) / (r_xy[0]-l_xy[0])
    phi = math.atan(tan) / math.pi * 180 # RAD->DEG, tan is pi-periodic
    phi_off = phi - fish_type.phi

    if l_xy[0] > r_xy[0]:
        # The image was scanned 180 with the wrong orientation.
        # Fortunately, we are clever enough to get you covered.
        # Since tan is pi-periodic, the angle is still correct.
        # We just need to update the position of the QR codes.
        log("Image is flipped, turning...")
        img = rotate_img_180(img)
        l_xy = (img.width - l_xy[0], img.height - l_xy[1])
        r_xy = (img.width - r_xy[0], img.height - r_xy[1])

    # shift the image so that the left qr code lies precisely where it,
    # as we are going to rotate the image around that point.
    # (We need to adjust these again after scaling.)
    x_off = fish_type.qr_xy_l[0] - l_xy[0]
    y_off = fish_type.qr_xy_l[1] - l_xy[1]

    # print or scan may be off scale, so we need to get the scaling
    # factor.
    scale = (fish_type.get_qr_dist() /
        math.sqrt((r_xy[0]-l_xy[0])**2 + (r_xy[1]-l_xy[1])**2))

    log("Adjusting scan size, rotation and position...")
    img = img.resize((int(img.width * scale), int(img.height * scale)))

    # The QR code position has also been scaled, so we need to adjust
    # that as well.
    x_off, y_off = x_off * scale, y_off * scale

    # rotate around left qr code...
    img = img.rotate(phi_off,
        center=l_xy,
        translate=(x_off, y_off),
        expand=False)

    # ... then crop to bounding box size.
    img = img.crop(fish_type.bounding_box)

    # knock out the background.
    img.putalpha(stencil)
    return img

def send_image(
        data : bytes,
        server_address : (str, int)) -> None:
    """
    Sends the image to the visualization server.
    """

    log(f"Sending {len(data)} bytes to {server_address[0]}:{server_address[1]}...")
    max_attempts = 5
    attempt = 1
    while attempt <= max_attempts:
        try:
           tcp_socket = socket.create_connection(server_address)
           tcp_socket.sendall(bytes(data))
           tcp_socket.close()
        except Exception as ex:
            log(f"Sending image failed. Reattempting. ({attempt}/{max_attempts})\n{ex}")
            attempt += 1
        else:
            log("Image sent.")
            break

def main_loop():
    if config.use_gpio:
        initialize_gpio()
    else:
        log("GPIO deactivated")

    if config.use_scanner:
        scanner = initialize_scanner()
    else:
        log("Scanner deactivated")

    while (True):
        await_scanner_triggered()
        scanned = scan_image(scanner) if config.use_scanner else fake_scan()
        try:
            fish_type, l_xy, r_xy = identify_image(scanned)
        except RuntimeError as e:
            log(f"Scan invalid: {e}")
            continue

        cut = cut_image(scanned, fish_type, l_xy, r_xy)

        log("Serializing scan data...")
        data = sltp_serialize(cut, fish_type.id)

        if config.use_network:
            send_image(data, (config.server_addr, config.server_port))
        else:
            log("Would send image, but networking is deactivated.")

        # sleep for a configured time to prevent people from spamming.
        time.sleep(config.delay_secs_after_scan)

if __name__ == "__main__":
    print("Malquarium by Turysaz")
    main_loop()
