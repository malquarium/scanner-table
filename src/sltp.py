
from PIL import Image

def sltp_serialize(img: Image.Image, fish_type : int) -> bytes:
    """
    Serializes the image data as SLTP v1
    """

    # helper funcs for creating byte arrays from ints
    def i8(x : int): return x.to_bytes(1, "little")
    def i16(x : int): return x.to_bytes(2, "little")
    def i32(x : int): return x.to_bytes(4, "little")

    imgdata = img.getdata()
    data = bytearray()

    # ================================
    # Sealife transmission protocol v1
    # ================================

    # Protocol Header (PHEAD)
    data.append(1)                  # protocol version
    data += b'\0\0\0'               # (padding)
    data += i32(len(imgdata) * 4)   # DATA length (4 byte/pixel)
    # Data Header (DHEAD)
    data += i8(fish_type)        # fish type
    data += b'\0\0\0'               # (padding)
    data += i16(img.width)          # image width
    data += i16(img.height)         # image height
    # DATA
    for px in imgdata:
        data += i8(px[0])      # R
        data += i8(px[1])      # G
        data += i8(px[2])      # B
        data += i8(px[3])      # A

    return bytes(data)
