
import config
import unittest
import malquarium as sut_1
import sltp as sut_2
from PIL import Image

class Scanner_Tests(unittest.TestCase):

    def test_fish_type_requires_L_mode_stencil(self):
        with self.assertRaises(ValueError):
            t = sut_1.FishType(
                0,
                "x",
                Image.new("RGBA", (3,3)), # wrong mode!
                (0,0,5,5),
                ((1,1),(2,2)))

    def test_fish_type_phi_calculation(self):
        t = sut_1.FishType(
            0,
            "x",
            Image.open("./stencils/fish-1.png"),
            (0,0,10,10),
            ((10,10),(20,20)))

        self.assertEqual(t.phi, 45)

    def test_fish_type_stencil_cropped_to_bounding_box(self):
        t = sut_1.FishType(
            0,
            "x",
            Image.open("./stencils/fish-1.png"),
            (2,2,10,10),
            ((10,10),(20,20)))

        self.assertEqual(t.stencil.width, 8)
        self.assertEqual(t.stencil.height, 8)
    
    @unittest.skipIf(
        config.use_scanner == False,
        "Scanner deactivated")
    def test_scan_resolution_matches_expected_dpi(self):
        scanner = sut_1.initialize_scanner()
        img : Image.Image = sut_1.scan_image(scanner)
        self.assertEqual(img.width, 885)
        self.assertEqual(img.height, 636)

    def test_identify_all_test_images(self):
        def testcase(file : str, id : int, x1, y1, x2, y2):
            img = Image.open(file)
            t, l_xy, r_xy = sut_1.identify_image(img)

            self.assertEqual(t.id, id)
            self.assertAlmostEqual(l_xy[0], x1, delta=5)
            self.assertAlmostEqual(l_xy[1], y1, delta=5)
            self.assertAlmostEqual(r_xy[0], x2, delta=5)
            self.assertAlmostEqual(r_xy[1], y2, delta=5)

        testcase("./testdata/scan-1-1.png", 1, 65, 90, 805, 566)
        testcase("./testdata/scan-1-2.png", 1, 75, 43, 814, 583)
        testcase("./testdata/scan-2-1.png", 2, 63, 95, 810, 561)
        testcase("./testdata/scan-3-1.png", 3, 65, 90, 805, 565)
        testcase("./testdata/scan-4-1.png", 4, 64, 88, 802, 566)
    
    def test_cut_image_as_expected(self):
        stencil = Image.new("L", (10,10), color="black")
        # todo : draw on stencil

        t = sut_1.FishType(
            id=99,
            qr_ident="foo",
            stencil=stencil,
            bounding_box=(1,1,9,9),
            qrcodes_xy=((1,1),(9,9)))

        self.fail()

    def test_serialize_image_as_expected(self):
        img = Image.new("RGBA", (3,3), color="black")
        stencil = Image.new("L", (3,3), color="black")
        t = sut_1.FishType(42, "", stencil, (0,0,1,1),((0,0),(1,1)))
        data = sut_2.sltp_serialize(img, t.id)
        self.assertEqual(
            data,
            bytes([
                  1, 0, 0, 0, 36, 0, 0, 0,
                 42, 0, 0, 0,  3, 0, 3, 0]
                 + [0,0,0,0xff] * 9))

if __name__ == "__main__":
    unittest.main(verbosity=3)